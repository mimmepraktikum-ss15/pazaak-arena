# PAZAAK ARENA #

This is a short readme-file to help you setting up the Pazaak Arena game. You will also find information about the used web technologies and the people involved in this project.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I set up the game? ###

Pazaak Arena Setup-Anleitung

Pazaak Arena läuft mit einer wechselseitigen Beziehung zwischen Node.js und MongoDB. Möchte man einen lokalen Server auf dem eigenen System einrichten, so muss man beide Technologien installieren.

Downloadlinks:
MongoDB: https://www.mongodb.org/downloads?_ga=1.170863264.1204946565.1442238252
Node.js: https://nodejs.org/en/download/

Anleitung:
Den Speicherort für die MongoDB muss man, in folgender Form „data/db“, auf dem Laufwerk anlegen, auf dem MongoDB installiert ist (z.B. "C:/data/db"). Anschließend führt man die mongod.exe aus.
Um den Node.js Server zu starten wird nun in den Pazaak Arena-Ordner navigiert (Beispielpfad: „D:\Uni\MME\Pazaak Arena“). Nun öffnet man die cmd.exe indem man mit Shift + Rechtsklick auf den Unterordner „/server“ geht und den Menüpunkt „Eingabeaufforderung hier öffenen“ wählt. Anschließend muss man "node app.js" eingeben und mit Enter bestätigen.
Um nun Pazaak Arena zu starten lädt man die Anwendung mit Hilfe eines beliebigen Browsers indem man die index.html Datei ausführt.


### What web technologies were used in the making of this project? ###

* jquery
* ember.js
* node.js
* mongoDB

### Who do I talk to? ###

* Michael Wax (michael.wax@stud.uni-regensburg.de)
* Laurin Schnell (laurin.schnell@stud.uni-regensburg.de)